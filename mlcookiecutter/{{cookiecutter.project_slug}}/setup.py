#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="{{ cookiecutter.project_slug }}",
    author="{{ cookiecutter.full_name.replace('\"', '\\\"') }}",
    author_email="{{ cookiecutter.email }}",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
    ],
    description="{{ cookiecutter.project_short_description }}",
    entry_points={"console_scripts": []},
    install_requires=[],
    license="Proprietary",
    include_package_data=True,
    keywords="{{ cookiecutter.project_slug }}",
    packages=find_packages("src"),
    package_dir={"": "src"},
    setup_requires=["pytest-runner"],
    test_suite="tests",
    tests_require=["pytest"],
    url="https://gitlab.com/droythorne/{{ cookiecutter.project_slug }}",
    version="{{ cookiecutter.version }}",  # Modify with bumpversion
)
