"""Tests for {{ cookiecutter.project_slug }} package."""

import pytest
from {{ cookiecutter.project_slug }} import {{ cookiecutter.project_slug }}


@pytest.fixture
def response():
    """Sample pytest fixture.
    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    return "response"


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    assert response == "response"
