Cookiecutter Template for ML Analysis
=====================================

`Cookiecutter`_ template for a machine learning project with `Pipenv`_ as the package manager.

Features
--------

Includes:

- testing setup with `pytest`_ 
- using `Bumpversion`_ for version bumping with a single command

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _Pipenv: https://docs.pipenv.org/
.. _pytest:
.. _Bumpversion: https://github.com/peritus/bumpversion

Quickstart
----------

Prerequisites
~~~~~~~~~~~~~

Install the latest Cookiecutter if you haven't installed it yet (this requires Cookiecutter 1.4.0 or higher):

.. code-block:: shell

    pip install --user cookiecutter

Install the latest Pipenv:

.. code-block:: shell

    pip install --user pipenv

Start a new project
~~~~~~~~~~~~~~~~~~~

Generate a Python project using the cookiecutter:

.. code-block:: shell

    cookiecutter https://bitbucket.org/thoughtriver/cookiecutter

Once your project has been created:

- create a git repository (e.g. :code:`git init`).
- install the dev requirements into a virtual environment (:code:`pipenv install --dev`).
- to run tests, install the package in editible mode within your environment with :code:`pipenv install -e .`
- run Makefile commands under Pipenv virtualenv (e.g. either :code:`pipenv run make test`, or :code:`pipenv shell && make test`)


